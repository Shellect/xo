from game import Game
from grid import Grid
from player import Player
from board import Board
from console import Console

(Game(
    Console(
        Grid(
            Player('X'),
            Player('O')
        )
    )
)).run()

import time


class Clock:
    def __init__(self, fps):
        self.fps = fps
        self.last_update = time.time()

    def tick(self):
        current_time = time.time()
        microseconds = 1000 / self.fps - (current_time - self.last_update)
        microseconds = max(microseconds, 0)
        time.sleep(microseconds / 1000)
        self.last_update = current_time

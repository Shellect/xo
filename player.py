class Player:
    def __init__(self, mark):
        self.mark = mark

    def __str__(self):
        return self.mark

    def __repr__(self):
        return self.mark

    def __nonzero__(self):
        return self.mark == 'X'
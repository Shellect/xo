import os
import math


class Console:
    def __init__(self, grid):
        self.grid = grid

    def take_input(self):
        player_choice = None
        while player_choice is None:
            player_choice = input("Выберите ячейку [1-9]\n>>> ")
            if player_choice.isdigit():
                player_choice = int(player_choice) - 1
            if player_choice < 0 or player_choice > 8:
                player_choice = None
        column = player_choice % 3
        row = player_choice // 3
        self.grid.mark(column, row)

    def update(self):
        os.system('cls')
        self.grid.print()

    def check_status(self):
        return not self.grid.check_win()

    def quit(self):
        self.update()
        input("Game over. Press [Enter] to exit...")


import numpy


class Grid:
    def __init__(self, *players):
        self.matrix = numpy.empty(shape=(3, 3), dtype=object)
        self.players = players
        self.current_player = 0

    def mark(self, column, row):
        if not self.matrix[row, column]:
            self.matrix[row, column] = self.players[self.current_player]
            self.current_player = (self.current_player + 1) % len(self.players)

    def check_win(self):
        return numpy.any(numpy.concatenate((
            numpy.all(self.matrix, axis=1),
            numpy.all(self.matrix, axis=0),
            [numpy.all(numpy.diagonal(self.matrix))],
            [numpy.all(numpy.diagonal(numpy.fliplr(self.matrix)))]
        )))

    def print(self):
        print(self.matrix)

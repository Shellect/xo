import math
from tkinter import Tk, Canvas
import os


class Board:
    size = 300
    bg_color = "#92877d"

    def __init__(self, grid):
        self.grid = grid
        self.root = self.__create_window()
        self.canvas = self.__create_canvas(self.root)

    def __create_window(self):
        root = Tk()
        root.title('XO')

        ws = root.winfo_screenwidth()  # width of the screen
        hs = root.winfo_screenheight()  # height of the screen

        x = (ws / 2) - (self.size / 2)  # x position of left top window corner
        y = (hs / 2) - (self.size / 2)  # y position of left top window corner

        root.geometry("%dx%d+%d+%d" % (self.size, self.size, x, y))
        root.resizable(False, False)

        root.bind('<Button-1>', self.__handle_click)
        root.protocol('WM_DELETE_WINDOW', self.__close_window)

        return root

    def __create_canvas(self, root):
        canvas = Canvas(root, width=self.size, height=self.size, bg=self.bg_color)
        canvas.pack()

        for i in range(3):
            for j in range(3):
                x = i * 100
                y = j * 100

                canvas.create_rectangle(
                    x, y,
                    x + self.size,
                    y + self.size,
                    outline="#333333",
                    width=3
                )

        return canvas

    def __close_window(self):
        self.game_process = False

    def mark(self, column, row):
        self.grid.mark(column, row)


    def __handle_click(self, event):
        self.event = event

    def update(self):
        self.root.update()

    def quit(self):
        self.root.quit()

    def key_handler(self):
        if self.event:
            column = math.floor(self.event.x / 100)
            row = math.floor(self.event.y / 100)
            self.board.mark(column, row)
            self.event = None
            return True
        return False
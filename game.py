from clock import Clock


class Game:
    def __init__(self, board):
        self.game_process = True
        self.board = board
        self.clock = Clock(60)
        self.event = None

    def __check_status(self):
        pass

    def run(self):
        while self.game_process:
            self.clock.tick()
            self.board.update()
            self.board.take_input()
            self.game_process = self.board.check_status()
        self.board.quit()
